import java.util.ArrayList;
import java.util.List;
public class state{
	
	//N floors, K lifts
	public int N;
	public int K;
	public int[] currentFloor; //K
	public boolean[] outsideUp; //N
	public boolean[] outsideDown; //N
	public boolean[][] buttonPressed; //K,N
	public int[] destination; //K
	public List<List<update>> updatelocal; //K
	public ArrayList<update> updateglobal;
	public boolean[] isFree; //K
	public boolean[] direction; //K
	
	public state(int N, int K){
		
		this.N =N;
		this.K =K;
		this.currentFloor = new int[K+1];
		
		for(int i=1; i<K+1; i++){
			this.currentFloor[i]=1;
		}
		
		this.outsideUp = new boolean[N+1];
		this.outsideDown = new boolean[N+1];
		this.buttonPressed = new boolean[K+1][N+1];
		this.destination = new int[K+1];
		this.updatelocal = new ArrayList<List<update>>(K+1); 
		
		for(int i = 0; i <K+1; i++)  {
            updatelocal.add(new ArrayList<update>());
        }
		
		this.updateglobal = new ArrayList<update>();
		this.isFree = new boolean[K+1];
		for(int i=1; i<K+1; i++){
			this.isFree[i]=true;
		}
		this.direction = new boolean[K+1];
	}

	public void print(){
		
		System.err.println("--------------destination------------");
		
		for(int i=1; i<this.destination.length; i++){
			System.err.print(destination[i] + " ");
		}
		System.err.println();
		System.err.println("---------000--destination----000--------");
		/*
		System.err.println("outsideUp " + this.outsideUp.toString());
		System.err.println("outsideDown " + this.outsideDown.toString());
		System.err.println("buttonPressed " + this.buttonPressed.toString());
		System.err.println("destination " + this.destination.toString());
		System.err.println("isFree " + this.isFree.toString());
		System.err.println("direction " + this.direction.toString());
		*/
	}
	
}
