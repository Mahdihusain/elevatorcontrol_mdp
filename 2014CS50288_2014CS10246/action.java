
public class action{
	
	public int actionType; //AU-1,AD-2,AOU-3,AOD-4,AS-0

	public int lift; //lift no.
	
	public action(int actionType, int lift){
		
		this.actionType = actionType;
		this.lift = lift;

		
	}
	
	@Override
	public String toString(){
		
		if(this.actionType==0){
			return "AS"+this.lift;
		}else if(this.actionType==1){
			return "AU"+this.lift;
		}else if(this.actionType==2){
			return "AD"+this.lift;
		}else if(this.actionType==3){
			return "AOU"+this.lift;
		}else{
			return "AOD"+this.lift;
		}
		
	} 
	
}
