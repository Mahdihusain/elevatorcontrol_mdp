import java.util.Scanner;

public class MDPAgent{
	
	public static Scanner scan = new Scanner(System.in);
	
	public static void main(String args[]){
		
		//pre computation of transition model
		
		System.out.println('0');
		System.out.flush();
		agent(args);
		
	}
	
	public static void agent(String args[]){
		
		int N = Integer.parseInt(args[0]);
		int K = Integer.parseInt(args[1]);
		double p = Double.parseDouble(args[2]);
		double q = Double.parseDouble(args[3]);
		double r = Double.parseDouble(args[4]);
		
		state s = new state(N,K);
		transitionFunction tf = new transitionFunction();
		policy pol = new policy((int) (p*100),(int) (q*100),(int) (r*100));
		
		String[] updates = scan.nextLine().trim().split(" ");
		
		for(int i=0; i<updates.length; i++){
			String[] u = updates[i].split("_");
			update up;
			
			if(u.length==2){
				if(u[0].equals("BU")){
					up = new update(1,0,Integer.parseInt(u[1]));
				}else{
					up = new update(2,0,Integer.parseInt(u[1]));
				}
				s.updateglobal.add(up);
				tf.funUpdate(s,up);
				
			}else if(u.length==3){
				up = new update(3, Integer.parseInt(u[2]),Integer.parseInt(u[1]));
				s.updatelocal.get(Integer.parseInt(u[2])).add(up);
				tf.funUpdate(s,up);
			}
			
		}
		
		while(true){
			
			
			action[] acts = pol.greedy(s);
			String act = "";
			
			for(int i=1; i<acts.length; i++){
				act += acts[i].toString();
				act += " ";
			}
			
			//System.err.println(act.trim());
			System.out.println(act.trim());
			System.out.flush();
			//tf.allfunAction(s, acts);
			
			//System.out.println("AS1 AS2");
			
			try{
				updates = scan.nextLine().trim().split(" ");
				
			}catch(Exception e){
				System.exit(0);
			}
			

			for(int i=0; i<updates.length; i++){
				//System.err.println(updates[i]);
				String[] u = updates[i].split("_");
				update up;
				if(u.length==2){
					
					if(u[0].equals("BU")){
						up = new update(1,0,Integer.parseInt(u[1]));
					}else{
						up = new update(2,0,Integer.parseInt(u[1]));
					}
					s.updateglobal.add(up);
					tf.funUpdate(s,up);
					
				}else if(u.length==3){
					
					up = new update(3, Integer.parseInt(u[2]),Integer.parseInt(u[1]));
					s.updatelocal.get(Integer.parseInt(u[2])).add(up);
					tf.funUpdate(s,up);
				}
				
			}
			
		}
		
	}
	
}
