import java.util.ArrayList;
import java.util.List;
public class transitionFunction{
	
	public state allfunAction(state s, action[] acts){
		
		//state newState = s.copy();
		
		for(int i=1; i<acts.length; i++){
			funAction(s, acts[i]);
		}
		
		return s;
	}
	
	public state allfunUpdate(state s, ArrayList<update> updates){
		
		//state newState = s.copy();
		
		for(int i=0; i<updates.size(); i++){
			funUpdate(s, updates.get(i));
		}
		
		return s;
	}
	
	public void funAction(state s, action a){
		
		if(a.actionType==1){ //AU
			
			s.currentFloor[a.lift]++;
			s.direction[a.lift] = true;
			
		}else if(a.actionType==2){ //AD

			s.currentFloor[a.lift]--;
			s.direction[a.lift] = false;
			
		}else if(a.actionType==3){ //AOU
			
			s.direction[a.lift] = true;
			s.outsideUp[s.currentFloor[a.lift]] = false;
			s.buttonPressed[a.lift][s.currentFloor[a.lift]] = false;
			
			
		}else if(a.actionType==4){ //AOD
			
			s.direction[a.lift] = false;
			s.outsideDown[s.currentFloor[a.lift]] = false;
			s.buttonPressed[a.lift][s.currentFloor[a.lift]] = false;
			
			
		}
	
	}
	
	public void funUpdate(state s, update u){
		
		//System.err.println(u.updateType);
		
		if(u.updateType==1){ //BU
			
			
			s.outsideUp[u.buttonFloor] = true;
			
		}else if(u.updateType==2){ //BD
			
			s.outsideDown[u.buttonFloor] = true;
			
		}else if(u.updateType==3){ //Bbl
			
			s.buttonPressed[u.lift][u.buttonFloor] = true;
			
		}
		
	}
	
}
