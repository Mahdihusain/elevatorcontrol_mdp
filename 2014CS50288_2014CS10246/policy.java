import java.util.Random;

public class policy{
	
	public int p;
	public int q;
	public int r;
	
	public policy(int p,int q, int r){
		this.p=p;
		this.q=q;
		this.r=r;
	}
	
	public boolean isFree(state s, int lift){
		return s.isFree[lift];
	}
	
	public boolean isLocalEmpty(state s, int lift){
		return s.updatelocal.get(lift).isEmpty();
	}
	
	public boolean isGlobalEmpty(state s){
		return s.updateglobal.isEmpty();
	}
	
	public action[] greedy(state s){
		
		transitionFunction tf = new transitionFunction();
		action[] act = new action[s.K+1];
		
		for(int i=1; i<=s.K; i++){
			
			act[i] = getAct(s,i);
			tf.funAction(s,act[i]);
		}
		
		return act;
	}
	
	public action assign(state s, int lift){
		
		action a;
		Random r = new Random();
		
		if(s.buttonPressed[lift][s.currentFloor[lift]]){
			if(s.outsideDown[s.currentFloor[lift]]){
				a = new action(4, lift);
			}else if(s.outsideUp[s.currentFloor[lift]]){
				a = new action(3, lift);
			}else if(s.currentFloor[lift]==1){
				a = new action(3, lift);
			}else if(s.currentFloor[lift]==s.N){
				a = new action(4, lift);
			}else{
				a = new action(4, lift);
			}
			
			return a;			
		}
		
		if(s.outsideDown[s.currentFloor[lift]]){
			a = new action(4, lift);
			return a;
		}
		
		if(s.outsideUp[s.currentFloor[lift]]){
			a = new action(3, lift);
			return a;
		}
		
		update u;
		
		/*
		if(s.outsideUp[1]){
			int  n = r.nextInt(100) + 1;
			
			if((this.p*this.q)/100>n){
				s.destination[lift] = 1;
				s.isFree[lift]=false;
				a = new action(2,lift);
				return a;
			}
			
		}
		
		if(s.buttonPressed[lift][1]){
			int  n = r.nextInt(100) + 1;
			
			if((this.p*this.r)/100>n){
				s.destination[lift] = 1;
				s.isFree[lift]=false;
				a = new action(2,lift);
				return a;
			}
		}*/
		
		if(!isLocalEmpty(s,lift)){
			
			while(!s.updatelocal.get(lift).isEmpty()){
				u = s.updatelocal.get(lift).remove(0);
				
				if(s.buttonPressed[lift][u.buttonFloor]){
					s.destination[lift] = u.buttonFloor;
					s.isFree[lift]=false;
					
					if(u.buttonFloor-s.currentFloor[lift]>0){
						a = new action(1, lift);
					}else if(u.buttonFloor-s.currentFloor[lift]<0){
						a = new action(2,lift);
					}else{
						
						if(s.outsideDown[s.currentFloor[lift]]){
							a = new action(4, lift);
						}else if(s.outsideUp[s.currentFloor[lift]]){
							a = new action(3, lift);
						}else if(s.currentFloor[lift]==1){
							a = new action(3, lift);
						}else if(s.currentFloor[lift]==s.N){
							a = new action(4, lift);
						}else{
							a = new action(4, lift);
						}
						
						s.isFree[lift]=true;
					}
					return a;					
				}
			}
	        
		}	
			
		if(!isGlobalEmpty(s)){
			
			while(!s.updateglobal.isEmpty()){
				u = s.updateglobal.remove(0);
				
			
				if(s.outsideUp[u.buttonFloor] || s.outsideDown[u.buttonFloor]){

					s.destination[lift] = u.buttonFloor;
					s.isFree[lift]=false;
					
					if(u.buttonFloor-s.currentFloor[lift]>0){
						a = new action(1, lift);
					}else if(u.buttonFloor-s.currentFloor[lift]<0){
						a = new action(2,lift);
					}else{
						if(s.outsideDown[s.currentFloor[lift]]){
							a = new action(4, lift);
						}else if(s.outsideUp[s.currentFloor[lift]]){
							a = new action(3, lift);
						}else if(s.currentFloor[lift]==1){
							a = new action(3, lift);
						}else if(s.currentFloor[lift]==s.N){
							a = new action(4, lift);
						}else{
							a = new action(4, lift);
						}
						s.isFree[lift]=true;
					}
					return a;					
					
				}
			}
			
		}
			
		a = new action(0, lift);
		
		return a;
	}
	
	public action getAct(state s, int lift){
		
		action a;
		
		if(isFree(s,lift)){
			
			return assign(s,lift);
			
		}else{
			
			if(s.buttonPressed[lift][s.destination[lift]] || s.outsideDown[s.destination[lift]] 
			|| s.outsideUp[s.destination[lift]]){
				
			}else{
				s.isFree[lift]=true;
				return assign(s,lift);
			}
			
			
			if(s.currentFloor[lift]==s.destination[lift]){
				
				if(s.outsideDown[s.currentFloor[lift]]){
					
					a = new action(4, lift);
					
				}else if(s.outsideUp[s.currentFloor[lift]]){
					
					a = new action(3,lift);
					
				}else{
					
					if(s.currentFloor[lift]==s.N){
						a = new action(4,lift);
					}else if(s.currentFloor[lift]==1){
						a = new action(3,lift);
					}else{
						a = new action(4,lift);
					}
					
				}
				
				s.isFree[lift]=true;
				
			}else{
				
				if(s.outsideDown[s.currentFloor[lift]] && !s.direction[lift]){
					
					a = new action(4,lift);
				}else if(s.outsideUp[s.currentFloor[lift]] && s.direction[lift]){
					a = new action(3 ,lift);
				}else if(s.buttonPressed[lift][s.currentFloor[lift]]){
					
					if(s.direction[lift]){
						a = new action(3,lift);
					}else{
						a = new action(4,lift);
					}
					
				}else{
					if(s.direction[lift]){
						a = new action(1,lift);
					}else{
						a = new action(2,lift);
					}
				}
			}
			
		}
		
		return a;
	}
	
}
